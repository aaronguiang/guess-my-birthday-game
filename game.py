from random import randint
my_name = print (input("Hi! What is your name?"))

num = 1

for guess_number in range(5):

    month = randint(1,12)
    year = randint(1924, 2004)

    guess_number = print(month, "/", year)

    guess_statement = print(my_name, "were you born in", month, "/", year)
    response = input("yes or no? ")


    if response == "yes":
        print("I knew it!")
        break

    elif response == "no":
        print("Drat! Lemme try again!")
        print(guess_statement)
        num = num + 1

    else:
        print("I have other things to do. Good bye.")
